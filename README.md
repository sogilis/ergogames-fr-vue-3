# Ergogames

Les Ergogames sont de petits jeux permettant de découvrir les principes fondamentaux d'ergonomie.

Le site est accessible à l'adresse [ergogames.fr](https://www.ergogames.fr).

## Installer l'environnement de travail

Premièrement, veuillez installer un environnement Node.js (via le système de paquets de votre système d'exploitation ou [via le site officiel](https://nodejs.org/en/download/)). La version supportée actuellement est **Node.js 10**.

Ensuite, il est nécessaire d'installer Yarn. Pour cela, veuillez vous référer à [la documentation officielle](https://yarnpkg.com/fr/docs/install).

Enfin, il ne vous reste plus qu'à installer les dépendances en ligne de commande avec :

```console
$ yarn install
```

Vous devriez désormais pouvoir lancer le site des Ergogames en local :

```console
$ yarn serve
```
## Environnement technique
Langages : essentiellement du Javascript (front : traiter les interactions avec l'utilisateur) + HTML (permet de créer le squelette de la page) + SCSS (sert à appliquer le graphisme)

Framework : Bootstrap, VueJS (le plus strusturant)

Netlify : hébergement

Gitlab : gestion des fichiers sources