import { expect } from 'chai';

import {
  addEntity,
  createEntity,
  getEntity,
  setEntityComponent,
  searchEntitiesAt,
  removeEntityComponent,
  removeEntity,
  getEntitiesAssets,
} from '@/views/games/pizzas/entity_system';

describe('entity_system utility functions', () => {
  describe('addEntity', () => {
    it('returns an array containing the given entity', () => {
      const store = [
        { id: 'A' },
      ];
      const entity = { id: 'B' };

      const updatedStore = addEntity(store, entity);

      expect(updatedStore).to.deep.eq([
        { id: 'A' },
        { id: 'B' },
      ]);
    });

    it('does not add entity if id already exists', () => {
      const store = [
        { id: 'A', value: 'foo' },
      ];
      const entity = { id: 'A', value: 'bar' };

      const updatedStore = addEntity(store, entity);

      expect(updatedStore).to.deep.eq([
        { id: 'A', value: 'foo' },
      ]);
    });

    it('fails if an entity has no id', () => {
      const store = [];
      const entity = { value: 'bar' };

      expect(() => addEntity(store, entity)).to.throw('entity object must have an id');
    });
  });

  describe('createEntity', () => {
    it('returns an entity by merging list of components', () => {
      const componentListA = { foo: 'bar', baz: 'bar' };
      const componentListB = { spam: 'egg' };

      const entity = createEntity(componentListA, componentListB);

      expect(entity).to.deep.eq({
        foo: 'bar',
        baz: 'bar',
        spam: 'egg',
      });
    });
  });

  describe('setEntityComponent', () => {
    it('returns store with the given entity updated', () => {
      const store = [
        { id: 'A', name: 'an entity' },
        { id: 'B', name: 'old entity' },
      ]
      const component = { name: 'new entity' };

      const result = setEntityComponent(store, 'B', component);

      expect(result).to.deep.eq([
        { id: 'A', name: 'an entity' },
        { id: 'B', name: 'new entity' },
      ]);
    });

    it('changes nothing if entity is not found', () => {
      const store = [
        { id: 'A', name: 'an entity' },
      ]
      const component = { name: 'new entity' };

      const result = setEntityComponent(store, 'B', component);

      expect(result).to.deep.eq([
        { id: 'A', name: 'an entity' },
      ]);
    });
  });

  describe('removeEntityComponent', () => {
    it('returns store with the given entity updated', () => {
      const store = [
        { id: 'A', name: 'an entity' },
      ]

      const result = removeEntityComponent(store, 'A', 'name');

      expect(result).to.deep.eq([
        { id: 'A' },
      ]);
    });
  });

  describe('getEntity', () => {
    it('returns the given entity from the list', () => {
      const store = [
        { id: 'A', name: 'an entity' },
        { id: 'B', name: 'another entity' },
      ]

      const result = getEntity(store, 'B');

      expect(result).to.deep.eq(
        { id: 'B', name: 'another entity' },
      );
    });

    it('returns null if entity is not found', () => {
      const store = [
        { id: 'A', name: 'an entity' },
      ]

      const result = getEntity(store, 'B');

      expect(result).to.be.null;
    });
  });

  describe('searchEntitiesAt', () => {
    it('returns entities present at a given position', () => {
      const position = { x: 2, y: 2 };
      const store = [
        { id: 'A', position },
      ];

      const result = searchEntitiesAt(store, position);

      expect(result).to.deep.eq([
        { id: 'A', position },
      ]);
    });

    it('returns entities considering its size', () => {
      const position = { x: 2, y: 2 };
      const store = [{
        id: 'A',
        position: { x: 1, y: 1 },
        size: { width: 2, height: 2 },
      }];

      const result = searchEntitiesAt(store, position);

      expect(result).to.deep.eq([{
        id: 'A',
        position: { x: 1, y: 1 },
        size: { width: 2, height: 2 },
      }]);
    });

    it('does not return not covered entities', () => {
      const position = { x: 2, y: 2 };
      const store = [{
        id: 'A',
        position: { x: 1, y: 1 },
        size: { width: 1, height: 1 },
      }];

      const result = searchEntitiesAt(store, position);

      expect(result).to.empty;
    });

    it('does not return entities without position', () => {
      const position = { x: 1, y: 1 };
      const store = [
        { id: 'A' },
      ];

      const result = searchEntitiesAt(store, position);

      expect(result).to.empty;
    });
  });

  describe('removeEntity', () => {
    it('removes the given entity from the list', () => {
      const store = [
        { id: 'A', name: 'an entity' },
        { id: 'B', name: 'another entity' },
      ]

      const result = removeEntity(store, 'B');

      expect(result).to.deep.eq([
        { id: 'A', name: 'an entity' },
      ]);
    });
  });

  describe('getEntitiesAssets', () => {
    it('returns image value', () => {
      const store = [
        { id: 'A', image: '/image.png' },
      ]

      const result = getEntitiesAssets(store);

      expect(result).to.deep.eq([
        '/image.png',
      ]);
    });

    it('returns assets value', () => {
      const store = [
        {
          id: 'A',
          assets: {
            a: '/image-a.png',
            b: '/image-b.png',
          },
        },
      ]

      const result = getEntitiesAssets(store);

      expect(result).to.deep.eq([
        '/image-a.png',
        '/image-b.png',
      ]);
    });

    it('returns "direction" assets value', () => {
      const store = [
        {
          id: 'A',
          assets: {
            a: {
              below: '/image-below.png',
              right: '/image-right.png',
              left: '/image-left.png',
              top: '/image-top.png',
            },
          },
        },
      ]

      const result = getEntitiesAssets(store);

      expect(result).to.deep.eq([
        '/image-below.png',
        '/image-right.png',
        '/image-left.png',
        '/image-top.png',
      ]);
    });
  });
});
