import { expect } from 'chai';

import {
  positionIsAccessibleFor,
} from '@/views/games/pizzas/systems';

describe('systems functions', () => {
  describe('positionIsAccessibleFor', () => {
    // We consider the following board game:
    // -------------------
    // | 1,1 | 2,1 | 3,1 |
    // -------------------
    // | 1,2 | 2,2 | 3,2 |
    // -------------------
    // | 1,3 | 2,3 | 3,3 |
    // -------------------

    it('returns true if entity can access the given position', () => {
      const entity = { position: { x: 2, y: 2 } };

      [
        { x: 2, y: 1 },
        { x: 1, y: 2 },
        { x: 3, y: 2 },
        { x: 2, y: 3 },
      ].forEach((givenPosition) => {
        const result = positionIsAccessibleFor(entity, givenPosition);
        expect(result).to.be.true;
      });
    });

    it('returns false if entity cannot access the given position', () => {
      const entity = { position: { x: 2, y: 2 } };

      [
        { x: 1, y: 1 },
        { x: 3, y: 1 },
        { x: 1, y: 3 },
        { x: 3, y: 3 },
      ].forEach((givenPosition) => {
        const result = positionIsAccessibleFor(entity, givenPosition);
        expect(result).to.be.false;
      });
    });

    it('returns false if entity has no positons', () => {
      const entity = { };
      const position = { x: 1, y: 2 };

      const result = positionIsAccessibleFor(entity, position);
      expect(result).to.be.false;
    });
  });
});
